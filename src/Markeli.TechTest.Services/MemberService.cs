﻿using Markeli.TechTest.Core;
using Markeli.TechTest.Models;
using Markeli.TechTest.Repositories.Abstractions;
using Markeli.TechTest.Services.Abstractions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Markeli.TechTest.Services
{
    public class MemberService : IMemberService
    {
        private readonly IMemberRepository _repository;

        public MemberService(IMemberRepository repository)
        {
            _repository = repository;
        }

        public async Task AddAsync(IEnumerable<Member> models)
        {
            await _repository.AddAsync(models);
        }

        public async Task<PagedResult<Member>> GetPagedAsync(int page, int pageSize)
        {
            return await _repository.GetPagedAsync(page, pageSize);
        }
    }
}
