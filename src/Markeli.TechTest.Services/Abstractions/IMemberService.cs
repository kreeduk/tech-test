﻿using Markeli.TechTest.Core;
using Markeli.TechTest.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Markeli.TechTest.Services.Abstractions
{
    public interface IMemberService
    {
        Task AddAsync(IEnumerable<Member> models);
        Task<PagedResult<Member>> GetPagedAsync(int page, int pageSize);
    }
}
