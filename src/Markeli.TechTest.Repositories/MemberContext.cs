﻿using Markeli.TechTest.Repositories.Models;
using Microsoft.EntityFrameworkCore;

namespace Markeli.TechTest.Repositories
{
    public class MemberContext : DbContext
    {
        public MemberContext(DbContextOptions<MemberContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MemberEntity>()
                .ToTable("Members");
        }
    }
}