﻿using Markeli.TechTest.Core;
using Markeli.TechTest.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Markeli.TechTest.Repositories.Abstractions
{
    public interface IMemberRepository
    {
        Task AddAsync(IEnumerable<Member> models);
        Task AddAsync(Member model);
        Task DeleteAsync(int id);
        Task<Member> GetAsync(int id);
        Task UpdateAsync(Member model);
        Task<PagedResult<Member>> GetPagedAsync(int page, int pageSize);
    }
}
