﻿namespace Markeli.TechTest.Repositories.Models
{
    public class MemberEntity : EntityBase
    {
        public int SchemeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
    }
}