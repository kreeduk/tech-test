﻿using AutoMapper;
using Markeli.TechTest.Models;
using Markeli.TechTest.Repositories.Abstractions;
using Markeli.TechTest.Repositories.Models;

namespace Markeli.TechTest.Repositories
{
    public class MemberRepository : Repository<Member, MemberEntity>, IMemberRepository
    {
        public MemberRepository(MemberContext context, IMapper mapper)
            : base(context, mapper)
        {
        }
    }
}
