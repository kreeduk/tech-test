﻿using AutoMapper;
using Markeli.TechTest.Core;
using Markeli.TechTest.Repositories.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Markeli.TechTest.Repositories
{
    public class Repository<TModel, TEntity>
        where TModel : ModelBase
        where TEntity : EntityBase
    {
        protected readonly MemberContext Context;
        private readonly IMapper _mapper;

        public Repository(MemberContext context, IMapper mapper)
        {
            Context = context;
            _mapper = mapper;
        }

        public async virtual Task AddAsync(TModel model)
        {
            var entity = _mapper.Map<TEntity>(model);

            Context.Set<TEntity>().Add(entity);

            await Context.SaveChangesAsync();
        }

        public async virtual Task AddAsync(IEnumerable<TModel> models)
        {
            var entities = _mapper.Map<List<TEntity>>(models);

            Context.Set<TEntity>().AddRange(entities);

            await Context.SaveChangesAsync();
        }

        public async virtual Task DeleteAsync(int id)
        {
            var model = await GetAsync(id);
            var entity = _mapper.Map<TEntity>(model);

            Context.Set<TEntity>().Remove(entity);

            await Context.SaveChangesAsync();

        }

        public async virtual Task<TModel> GetAsync(int id)
        {
            var entity = await Context.Set<TEntity>().AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
            var model = _mapper.Map<TModel>(entity);

            return model;
        }

        public async virtual Task<IEnumerable<TModel>> GetAllAsync()
        {
            var entities = await Context.Set<TEntity>().AsNoTracking().ToListAsync();
            var models = _mapper.Map<List<TModel>>(entities);

            return models;
        }

        public async virtual Task UpdateAsync(TModel model)
        {
            var entity = _mapper.Map<TEntity>(model);

            Context.Set<TEntity>().Update(entity);

            await Context.SaveChangesAsync();
        }

        public async virtual Task<PagedResult<TModel>> GetPagedAsync(int page, int pageSize)
        {
            var count = await Context.Set<TEntity>().AsNoTracking().CountAsync();
            var entities = await Context.Set<TEntity>().AsNoTracking().Skip(page * pageSize).Take(pageSize).ToListAsync();
            var models = _mapper.Map<List<TModel>>(entities);

            return new PagedResult<TModel>
            {
                Results = models,
                RowCount = count
            };
        }
    }
}
