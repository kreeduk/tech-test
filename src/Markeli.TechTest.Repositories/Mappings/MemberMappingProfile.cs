﻿using AutoMapper;
using Markeli.TechTest.Models;
using Markeli.TechTest.Repositories.Models;

namespace Markeli.TechTest.Repositories.Mappings
{
    public class MemberMappingProfile : Profile
    {
        public MemberMappingProfile()
        {
            CreateMap<MemberEntity, Member>()
                .ForMember(dest => dest.EmailAddress, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.MobileNumber, opt => opt.MapFrom(src => src.Mobile))
                .ReverseMap();
        }
    }
}
