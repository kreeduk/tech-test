﻿using System.Collections.Generic;

namespace Markeli.TechTest.Web.Models
{
    public class UploadResult<T>
    {
        public IEnumerable<T> Results { get; set; }
        public IEnumerable<CsvLineError> Errors { get; set; }
    }
}
