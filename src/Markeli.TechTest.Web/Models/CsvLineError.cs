﻿using System.Collections.Generic;

namespace Markeli.TechTest.Web.Models
{
    public class CsvLineError
    {
        public int LineNumber { get; set; }
        public IEnumerable<string> ErrorMessages { get; set; }
    }
}
