﻿namespace Markeli.TechTest.Web.Models
{
    public class MemberListItemModel
    {
        public int Id { get; set; }
        public int SchemeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string MobileNumber { get; set; }
    }
}
