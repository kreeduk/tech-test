﻿using System.ComponentModel.DataAnnotations;

namespace Markeli.TechTest.Web.Models
{
    public class MemberCsvModel
    {
        [Required(ErrorMessage = "Scheme Id must have a value")]
        [Range(0, int.MaxValue, ErrorMessage = "Scheme Id must be an integer number")]
        public string SchemeId { get; set; }

        [Required(ErrorMessage = "First Name must have a value")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last Name must have a value")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Email Address must have a value")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Invalid Email Address")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "Mobile Number must have a value")]
        public string MobileNumber { get; set; }
    }
}
