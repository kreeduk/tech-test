export interface Member {
  id: number
  schemeId: number;
  firstName: string;
  lastName: string;
  emailAddress: string;
  mobileNumber: string;
}
