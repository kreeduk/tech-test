import { Component, ViewChild } from '@angular/core';
import { MemberGridComponent } from './components/member-grid/member-grid.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})

export class AppComponent {
  @ViewChild(MemberGridComponent, { static: false }) memberGrid: MemberGridComponent

  title = 'ClientApp';

  constructor() {
  }  

  onUploaded() {    
    this.memberGrid.loadData();    
  }
}
