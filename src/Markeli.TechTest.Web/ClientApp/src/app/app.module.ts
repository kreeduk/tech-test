import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { MemberService } from './services/member.service';
import { HttpClientModule } from '@angular/common/http';
import { MemberGridComponent } from './components/member-grid/member-grid.component';
import { MatTableModule } from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    FileUploadComponent,
    MemberGridComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MatTableModule
  ],
  providers: [MemberService],
  bootstrap: [AppComponent]
})
export class AppModule { }
