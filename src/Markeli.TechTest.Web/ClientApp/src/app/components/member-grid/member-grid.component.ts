import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Member } from '../../models/member.model';
import { MemberService } from '../../services/member.service';

@Component({
  selector: 'app-member-grid',
  templateUrl: './member-grid.component.html',
  styleUrls: ['./member-grid.component.sass']
})
export class MemberGridComponent implements OnInit {
  models: Member[];
  displayedColumns: string[] = ['id', 'schemeId', 'firstName', 'lastName', 'emailAddress', 'mobileNumber'];

  constructor(private memberService: MemberService, private changeDetectorRef: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.memberService.getAll().subscribe(
      x => {
        this.models = x;
        this.changeDetectorRef.detectChanges();
      }
    );
  }
}
