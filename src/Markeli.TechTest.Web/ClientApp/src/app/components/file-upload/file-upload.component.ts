import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MemberService } from '../../services/member.service';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.sass']
})
export class FileUploadComponent implements OnInit {
  @Output() uploaded = new EventEmitter();
  form: FormGroup;
  error: string;
  uploadResponse = { status: '', message: '', filePath: '' };

  constructor(private formBuilder: FormBuilder, private memberService: MemberService) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      member: ['']
    });
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];

      this.form.get('member').setValue(file);
    }
  }

  onSubmit() {
    const formData = new FormData();

    formData.append('file', this.form.get('member').value);

    this.memberService.upload(formData).subscribe(
      (res) => this.uploadResponse = res,
      (err) => this.error = err
    );

    this.uploaded.emit();
  }
}
