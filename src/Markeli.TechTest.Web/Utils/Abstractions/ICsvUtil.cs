﻿using Markeli.TechTest.Web.Models;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Markeli.TechTest.Web.Utils.Abstractions
{
    public interface ICsvUtil<TModel>
    {
        Task<UploadResult<TModel>> ConvertAsync(IFormFile file);
    }
}
