﻿using AutoMapper;
using Markeli.TechTest.Web.Models;
using Markeli.TechTest.Web.Utils.Abstractions;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Markeli.TechTest.Web.Utils
{
    public class CsvUtil<TModel> : ICsvUtil<TModel>
    {
        private readonly IMapper _mapper;

        public CsvUtil(IMapper mapper)
        {
            _mapper = mapper;
        }

        public async Task<UploadResult<TModel>> ConvertAsync(IFormFile file)
        {
            var models = new List<TModel>();
            var lineErrors = new List<CsvLineError>();
            var lineNumber = 0;

            using (var reader = new StreamReader(file.OpenReadStream()))
            {
                string line;

                while ((line = await reader.ReadLineAsync()) != null)
                {
                    lineNumber++;

                    var lineItemArray = line.TrimEnd('|').Split('|');
                    var lineItem = _mapper.Map<TModel>(lineItemArray);
                    var lineErrorResult = CheckLineIsValid(lineItem);

                    if (!lineErrorResult.Item1)
                    {
                        lineErrors.Add(new CsvLineError
                        {
                            LineNumber = lineNumber,
                            ErrorMessages = lineErrorResult.Item2.Select(x => x.ErrorMessage)
                        });

                        continue;
                    }

                    models.Add(lineItem);                    
                }
            }

            return new UploadResult<TModel>
            {
                Results = models,
                Errors = lineErrors
            };
        }

        private Tuple<bool, List<ValidationResult>> CheckLineIsValid(TModel lineItem)
        {
            var context = new System.ComponentModel.DataAnnotations.ValidationContext(lineItem, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(lineItem, context, results);

            return new Tuple<bool, List<ValidationResult>>(isValid, results);
        }
    }
}
