﻿using AutoMapper;
using Markeli.TechTest.Web.Models;

namespace Markeli.TechTest.Web.Mappings.Csv
{
    public class MemberCsvModelMappingProfile : Profile
    {
        public MemberCsvModelMappingProfile()
        {
            CreateMap<string[], MemberCsvModel>()
                .ForMember(dest => dest.SchemeId, opt => opt.MapFrom(src => src[0]))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src[1]))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src[2]))
                .ForMember(dest => dest.MobileNumber, opt => opt.MapFrom(src => src[3]))
                .ForMember(dest => dest.EmailAddress, opt => opt.MapFrom(src => src[4]));
        }
    }
}
