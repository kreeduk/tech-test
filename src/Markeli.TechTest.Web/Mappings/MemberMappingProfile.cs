﻿using AutoMapper;
using Markeli.TechTest.Models;
using Markeli.TechTest.Web.Models;

namespace Markeli.TechTest.Web.Mappings
{
    public class MemberCsvMappingProfile : Profile
    {
        public MemberCsvMappingProfile()
        {
            CreateMap<MemberCsvModel, Member>()
                .ForMember(dest => dest.Id, opt => opt.Ignore());

            CreateMap<MemberListItemModel, Member>()
                .ReverseMap();
        }
    }
}
