﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Markeli.TechTest.Models;
using Markeli.TechTest.Services.Abstractions;
using Markeli.TechTest.Web.Models;
using Markeli.TechTest.Web.Utils.Abstractions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Markeli.TechTest.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MemberController : ControllerBase
    {
        private readonly IMemberService _memberService;
        private readonly IMapper _mapper;
        private readonly ICsvUtil<MemberCsvModel> _csvUtil;

        public MemberController(IMemberService memberService, IMapper mapper, ICsvUtil<MemberCsvModel> csvUtil)
        {
            _memberService = memberService;
            _mapper = mapper;
            _csvUtil = csvUtil;
        }

        // GET: api/Member
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result = await _memberService.GetPagedAsync(0, 200);
            var models = _mapper.Map<List<MemberListItemModel>>(result.Results);

            return Ok(models);
        }

        // GET: api/Member/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Member
        [HttpPost]
        public void Post([FromBody] string value)
        {

        }

        [HttpPost("UploadFile")]
        public async Task<IActionResult> Post(IFormFile file)
        {
            if (file.Length <= 0)
            {
                return StatusCode(204);
            }

            var result = await _csvUtil.ConvertAsync(file);
            var domainModels = _mapper.Map<List<Member>>(result.Results);

            await _memberService.AddAsync(domainModels);

            return Ok(result);
        }

        // PUT: api/Member/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
