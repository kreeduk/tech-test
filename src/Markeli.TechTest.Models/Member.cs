﻿using Markeli.TechTest.Core;

namespace Markeli.TechTest.Models
{
    public class Member : ModelBase
    {
        public int SchemeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string MobileNumber { get; set; }
    }
}
