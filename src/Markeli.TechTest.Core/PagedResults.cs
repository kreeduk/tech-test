﻿using System.Collections.Generic;

namespace Markeli.TechTest.Core
{
    public class PagedResult<T>
    {
        public IEnumerable<T> Results { get; set; }
        public int RowCount { get; set; }
    }
}
